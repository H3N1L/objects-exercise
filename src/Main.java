public class Main {

    public static void main(String[] args) {
        // write your code here

        Pet marvin = new Pet("Marvin", "Dog");

        Person bob = new Person("Bob", "Smith");
        Person sarah = new Person("Sarah", "Smith");

        sarah.setMyPet(marvin);
        System.out.println(bob.getFullName());
        System.out.println(sarah.getFullName());

        sarah.setSurname("Jones");

        System.out.println(sarah.getFullName());

        Pet theirPet = sarah.getMyPet();

        if (theirPet == null) {
            System.out.println(sarah.getFirstName() + " has no pet");

        } else {
            System.out.println(sarah.getFirstName() + " has a pet called " + theirPet.getName());

        }


    }
}
