public class Person {

    //attributes
    private String firstName;
    private String surname;
    private Pet myPet;

    // constructor
    public Person(String firstName, String surname) {
        this.firstName = firstName;
        this.surname = surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getFullName() {
        return firstName + " " + surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setMyPet(Pet myPet) {
        this.myPet = myPet;
    }

    public Pet getMyPet() {
        return myPet;
    }
}
